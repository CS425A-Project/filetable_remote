import transaction
from ZODB import FileStorage, DB
import sys

length = 15

def init_connection(database_file_path):
	storage = FileStorage.FileStorage(database_file_path, read_only=True)
	db = DB(storage)
	connection = db.open()
	filetable_remote = connection.root()
	return filetable_remote

def format_string(unformatted_string):
	lines_num = int(len(unformatted_string)/length)
	formatted_string = []
	for i in range(lines_num):
		formatted_string.append(unformatted_string[i*length:i*length+length])
	formatted_string.append(unformatted_string[lines_num*length:lines_num*length + length])
	return formatted_string

def print_table(table):
	if len(table.keys()) != 0:
		print(''.join(['-']*(3*length+4)))
	for key in table.keys():
		curr = format_string(key)
		next = format_string(table[key][0])
		ip = format_string(str(table[key][1]))

		line_max = max(len(curr), len(next), len(ip))
		for i in range(line_max):
			print('|', end="")
			if len(curr) > i:
				print(curr[i]+ ''.join([' ']*(length-len(curr[i]))), end="")
			else:
				print(''.join([' ']*length), end="")
			print('|', end="")
			if len(next) > i:
				print(next[i]+ ''.join([' ']*(length-len(next[i]))), end="")
			else:
				print(''.join([' ']*length), end="")
			print('|', end="")
			if len(ip) > i:
				print(ip[i]+''.join([' ']*(length-len(ip[i]))), end="")
			else:
				print(''+''.join([' ']*length), end="")
			print('|')
		print(''.join(['-']*(3*length+4)))

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print('Error: Invalid arguments.')
		exit(0)
	db_file_path = sys.argv[1]
	filetable_remote = init_connection(db_file_path)
	if 'filetable' not in filetable_remote.keys():
		print('Error: Database empty.')
		exit(0)
	if len(sys.argv) > 2:
		if sys.argv[2] == 'print':
			print_table(filetable_remote['filetable'])