import transaction
from ZODB import FileStorage, DB
import sys

database_file_path = 'database_files/filetable_remote.db'

def init_database():
	storage = FileStorage.FileStorage(database_file_path)
	db = DB(storage)
	connection = db.open()
	filetable_remote = connection.root()
	return filetable_remote

def init_table(filetable_remote):
	filetable_remote['filetable'] = {}
	transaction.commit()

def insert_info(filetable_remote, curr_filename, next_filename, ip_addr, port):
	if curr_filename in filetable_remote['filetable'].keys():
		assert len(filetable_remote['filetable'][curr_filename]) != 0
		if filetable_remote['filetable'][curr_filename][0] != next_filename:
			return False
		for ip_tuple in filetable_remote['filetable'][curr_filename][1]:
			if ip_tuple[0] == ip_addr:
				return False
		filetable_remote['filetable'][curr_filename][1].append((ip_addr, port))
	else:
		filetable_remote['filetable'][curr_filename] = (next_filename, [(ip_addr, port)])
	filetable_remote._p_changed = 1
	transaction.commit()
	return True

def del_info(filetable_remote, filename):
	if filename in filetable_remote['filetable'].keys():
		data = {
		'val': filetable_remote['filetable'][filename][0],
		'ip': filetable_remote['filetable'][filename][1]
		}
		filetable_remote['filetable'].pop(filename, None)
		return data
	else:
		return {
		'val': None
		}

def get_info(filetable_remote, filename):
	if filename in filetable_remote['filetable'].keys():
		return {
		'val': filetable_remote['filetable'][filename][0],
		'ip': filetable_remote['filetable'][filename][1]
		}
	else:
		return {
		'val': None
		}

if __name__ == '__main__':
	filetable_remote = init_database()
	if len(sys.argv) > 1 and sys.argv[1] == 'init':
		init_table(filetable_remote)
	print(insert_info(filetable_remote, 'blah-part1', 'blah-part2', '192.168.1.1', 1232))
	print(insert_info(filetable_remote, 'blah-part1', 'blah-part2', '192.168.1.2', 1232))
	print(insert_info(filetable_remote, 'blah-part2', 'blah-part3', '192.168.1.2', 12321))
	print(insert_info(filetable_remote, 'blah-part1', 'next-file' , '192.168.1.2', 12321))
	print(filetable_remote['filetable'])